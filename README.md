# Demonstration of SQL-Injection and SQL-Injection Mitigation #

The goal of this project was to create a vulnerable web application susceptible to SQL Injection. The server
running underneath this application is nginx-1.10.3 and it makes use of the ModSecurity Web Application Firewall.
The directory structure for this project is defined in the section below.

### Ginger.io Homework Directory Structure ###

* This repository contains 3 folders, in order to understand it better the directory structure is as follows:
  
  **app** - contains index.php (our vulnerable application) and 2 sub directories css, and error. These contain 
  the stylesheet for the application and the error pages respectively.
  
  **modsecurity_conf** - this contains the modsecurity.conf file as well as the rules directory it imports the rules 
  from. The rules directory has one file named SQLRules.conf. These are the rules that I have written in order to 
  prevent sql injection on the vulnerable application.

  **nginx_conf** - This directory contains the nginx.conf file which is required for maintaining he configuration for
  nginx. It shows that ModSecurity has been enabled and modsecurity.conf is being imported. It also enables PHP.


### Set Up ###

* These set-up instructions assume that you already have an nginx server insalled with ModSecurity.
Copy the files in this order: place all files in the app directory inside the /usr/local/nginx/html
directory. 
* After this replace your existing modsecurity.conf file with the one provided in the 
modsecurity_conf directory. Also place the rules directory in the same directory i.e. /usr/local/nginx/conf
* Once this is done replace nginx.conf in the same directory with the one provided in the nginx_conf directory.
Once again these are located at /usr/local/nginx/conf
* Restart nginx using 
```
#!bash

systemctl restart nginx
```
If all goes well, nginx will restart without any errors. You now have modsecurity WAF protecting the application!

* Replace the $dbuser, $dbname and $dbpass variables in index.php with the appropriate values prior to getting started.
* The next section contains screenshots of me performing sql injection on this application.

### Demonstration of SQL Injection on the Vulnerable Web-Server ###

In this section, I shall demonstrate an INSERT based sql injection on our vulnerable web application. Since this was a whitebox
security assessment, I knew how the application worked and where it was vulnerable prior to performing these tests.

* **SQL Injection test 1**
  The quotation test. In this test, I provided a single quotation mark (') as an input to the application in order to test sql injection.
  The results of this can be seen in the image below.
 ![SQLTest2.PNG](https://bitbucket.org/repo/AgGEBB8/images/147911901-SQLTest2.PNG)
  
As seen in the image, after passing in a quotation mark, the application responded with an error which gave out information about the
  database. This proves that the application is vulnerable to sql injection.

* **SQL Injection test 2: extracting Mysql version**
   In this test I demonstrate how to extract the Mysql version from the application using SQL injection. I constructed a malicious query
   as shown in the figure below.
![VersionTest1.PNG](https://bitbucket.org/repo/AgGEBB8/images/2714466218-VersionTest1.PNG)

After the above malicious query executed at the backend, I was rewarded with the MySQL version that the web-server was running. This can be 
seen from the following image:
![VersionTest2.PNG](https://bitbucket.org/repo/AgGEBB8/images/4103172919-VersionTest2.PNG)
As we can see from the above image, the MySQL version has been exposed, meaning that our query successfully executed at the backend. Thus, 
I made use of the subquery feature of MySQL in order to exploit the database and obtain the version.

* **SQL Injection test 3: extracting the root authentication string**
Since this was a whitebox test, I knew the structure of the database and what I would need to do in order to get the root authentication string.
For this purpose, I created a malicious query as shown in the image below. Since I knew that the database user was root, I could extract the 
authentication string by passing in the following query:
![rootTest1.PNG](https://bitbucket.org/repo/AgGEBB8/images/3731162526-rootTest1.PNG)

Now that I had constructed my query all that was left to do was hit submit and be rewarded with the authentication string as shown in the 
image below:
![Roottest2.PNG](https://bitbucket.org/repo/AgGEBB8/images/3873302426-Roottest2.PNG)

Thus from the three tests demonstrated above, I showed how vulnerable the application is to Insert Query based SQL Injection. The final step in this
project was to write my own firewall rules. These can be found under **/modsecurity_conf/rules/SQLRules.conf**

### References ###

 - [The OWASP Core Rule Set for ModSecurity](https://github.com/SpiderLabs/owasp-modsecurity-crs)
 
### Contact ###

* Purushottam Kulkarni
* puru1761@gmail.com