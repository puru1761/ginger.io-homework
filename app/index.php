<?php

$text = $_GET['input_text'];
# The following line is commented out to make the application vulnerable.
# This can be uncommented in order to add an additional layer of security.

#$text = mysqli_real_escape_string($text); # Uncomment to harden.

$dbhost = '127.0.0.1:3306';

# Another possible security measure could be creating a database user for
# the application in question say 'sqli' and having only that user access
# The database. 
$dbuser = 'root'; 

$dbpass = ''; 
# I have not included the database password in this submission for protecting my instance
$dbname = 'sqli';

$conn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
if (!$conn) {
	die('Could not connect:'.mysqli_error());
}

#-------------------------------------------
# Rendering og the html page through php.
#------------------------------------------
echo '
<html>
<head>
	<title> Ginger.io Homework </title>
	<link rel="stylesheet" type="text/css" href="/css/gingerStyle.css">
</head>

<body>
	<h1> <center> This is my Nginx Server for Ginger.io! </center> </h1>
	<h2> <center> Please enter a String in the input field given below </center> </h2>
	<form method>
		<fieldset> <center>
			<h3>  Enter a String: </h3>
			<input type="text" name="input_text"> <br><br>
			<input type="submit" value="Submit"> </center>
		</fieldset>
	</form>';
# --------------------------------------------------------------------
# Retrieving the list from the backend.
#----------------------------------------------------------------------
if ($text != '') {
	$query = 'insert into UserStr (string) values (\''.$text.'\');';
	$result = mysqli_query($conn,$query);

	if(!$result) {
		$message = 'Entered query: <b>'.$text."</b><br>";
		$message .= 'Invalid query: '.mysqli_error($conn)."\n";
		die($message);
	}

	$query = 'select * from UserStr;';
	$result = mysqli_query($conn, $query);

	if(!$result) {
		$message = '<p>Invalid query: '.mysqli_error($conn)."</p>\n";
		die($message);
	}
	echo '<center><table>';
	echo '<tr><th><center> Strings From Backend </center></th></tr>';
	while($row = mysqli_fetch_assoc($result)){
		echo '<tr><td><center>'.$row['string'].'</center></td></tr>';
	}
	echo '</table></center>';
}
echo '</body> </html>';
mysqli_close($conn);
?>
